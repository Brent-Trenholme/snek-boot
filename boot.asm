BOOT_MEM equ 0x7c00 ; Memory segment that boot sector is loaded into

; Default dimensions for VGA text mode
VGA_WIDTH       equ 80
TWICE_VGA_WIDTH equ VGA_WIDTH * 2
VGA_HEIGHT      equ 25

; Interrupt codes for writing characters to screen
VIDEO_INT          equ 0x10
VIDEO_CLEAR        equ 0x06
VIDEO_CURSOR_SHAPE equ 0x01

; Interrupt codes to read from keyboard
KEY_INT   equ 0x16
KEY_READ  equ 0x00
KEY_QUERY equ 0x01

; Interrupt codes to get system tick count
TIME_INT   equ 0x1A
TIME_TICKS equ 0x00

; Arrow key scan codes
LEFT_SCAN  equ 75
RIGHT_SCAN equ 77
UP_SCAN    equ 72
DOWN_SCAN  equ 80

%define vga_char(attr, ascii) ((attr << 8) + ascii)

SNEK_CHAR  equ vga_char(0x0A, '#')
APPLE_CHAR equ vga_char(0x0C, '@')
CLEAR_CHAR equ vga_char(0x00, ' ')

; Set address organisation to BOOT segmentation
org BOOT_MEM

; Start of sector
sector:
    ; jump to program setup
    jmp setup
    nop

    ; BPB stub for compatibility on some computers
.bpb:
    times 3-($-$$) db 0
    .bpb_oem_id:            db "Snek    "
    .bpb_sector_size:       dw 512
    .bpb_sects_per_cluster: db 0
    .bpb_reserved_sects:    dw 0
    .bpb_fat_count:         db 0
    .bpb_root_dir_entries:  dw 0
    .bpb_sector_count:      dw 0
    .bpb_media_type:        db 0
    .bpb_sects_per_fat:     dw 0
    .bpb_sects_per_track:   dw 18
    .bpb_heads_count:       dw 2
    .bpb_hidden_sects:      dd 0
    .bpb_sector_count_big:  dd 0
    .bpb_drive_num:         db 0
    .bpb_reserved:          db 0
    .bpb_signature:         db 0
    .bpb_volume_id:         dd 0
    .bpb_volume_label:      db "Snek       "
    .bpb_filesystem_type:   times 8 db 0

; Uninitialised Variables
rng_state equ 0x1000 ; Rng state
velocity  equ 0x1002 ; Initialise velocity as 0
apple_pos equ 0x1004 ; Apple position

; Initialised Variables
last_tick:  db 0 ; Lowest byte of the last tick count, initialised as 0
just_grown: db 0 ; Initialise just_grown status as false

; Setup program
setup:
    ; Setup tiny memory model
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov ss, ax

    ; Set gs to VGA memory
    mov ax, 0xb800
    mov gs, ax
    
    ; Setup stack to grow backwards from program start
    mov bp, BOOT_MEM

    ; Hide cursor
    mov ah, VIDEO_CURSOR_SHAPE
    mov ch, 0x20
    mov cl, 0x20
    int VIDEO_INT

    ; Seed rng state with time information
    mov ah, TIME_TICKS
    int TIME_INT
    mov [rng_state], dx

; Start of program
start:
    mov sp, bp             ; (re)Set stack pointer
    mov word [velocity], 0 ; (re)Set velocity to 0

    ; Clear screen
    mov ah, VIDEO_CLEAR
    xor al, al
    xor bh, bh
    xor cx, cx
    mov dx, ((VGA_HEIGHT - 1) << 8) + VGA_WIDTH - 1
    int VIDEO_INT

    ; Initialise snek head
    call random_position ; Generate random position
    push bx              ; Store position on stack

    ; Initialise apple
    call random_position ; Generate random position
    mov [apple_pos], bx  ; Store position

; Main loop
loop:
    mov ah, KEY_QUERY
    int KEY_INT
    jz no_keypress

    mov ah, KEY_READ
    int KEY_INT

    mov bx, [velocity]

    mov cx, 0x00ff
    cmp ah, LEFT_SCAN
    cmove bx, cx

    mov cx, 0x0001
    cmp ah, RIGHT_SCAN
    cmove bx, cx

    mov cx, 0xff00
    cmp ah, UP_SCAN
    cmove bx, cx

    mov cx, 0x0100
    cmp ah, DOWN_SCAN
    cmove bx, cx

    mov [velocity], bx

no_keypress:

    mov ah, TIME_TICKS
    int TIME_INT
    mov bl, dl          ; Backup lower tick byte
    sub dl, [last_tick] ; Get tick delta
    cmp dl, 2           ; If tick delta is greater or equal to 2 perform update
    jl loop             ; This will make the game update ~9 times per second
    mov [last_tick], bl ; Store new lower tick byte into last_tick

update:
    ; Clear current tail position if we didn't grow last update
    cmp byte [just_grown], 1
    je skip_tail_erase
    mov bx, sp
    mov bx, [bx]
    call clear_char

skip_tail_erase:
    ; Clear grown status since we've handled it
    mov [just_grown], byte 0

    ; Update segment positions
segments:
    mov di, sp          ; Load pointer to tail segment in stack
    mov si, sp
    add si, 2           ; Load pointer to segment before tail
segment_loop:
    cmp si, bp          ; Check if there is a previous segment
    je segment_loop_end ; If there isn't a previous segment, exit loop
    movsw               ; Write previous segment to current segment and inc pointers
    jmp segment_loop
segment_loop_end:

    ; Update head position
    mov bx, [bp - 2]       ; Load head position
    add bl, [velocity]     ; Add movement value to position
    add bh, [velocity + 1]
    call wrap_position     ; Wrap position
    mov [bp - 2], bx       ; Store new position

    ; Draw new head position
    mov ax, SNEK_CHAR
    call write_char

    ; Restart if head position is equal to any other segment position
    mov cx, bp       ; Store head address in cx
    sub cx, 2
    mov dx, [bp - 2] ; Store head position in dx
    mov bx, sp       ; Store tail position in bx
collision_loop:
    cmp bx, cx       ; If segment pointer equals head pointer, end loop
    je collision_end
    mov ax, [bx]
    cmp ax, dx       ; If segment position equals head poisition, restart
    je start
    add bx, 2
    jmp collision_loop
collision_end:

    ; If head position is equal to apple position, grow snek, and make new apple
    mov ax, [bp - 2]
    cmp ax, [apple_pos]
    jne skip_grow

    ; Grow snek
    mov bx, sp
    push word [bx]
    mov [just_grown], byte 1

    ; Make new apple
    call random_position ; Get random position
    mov [apple_pos], bx  ; Store position

skip_grow:

    ; Ensure apple is visible
    mov ax, APPLE_CHAR
    mov bx, [apple_pos]
    call write_char

    jmp loop

; Clear character at row bh and col bl
clear_char:
    mov ax, CLEAR_CHAR
; Write charater ax at row bh and col bl
write_char:
    ; Move character info into cx
    mov cx, ax

    mov al, bl
    xor ah, ah
    shl ax, 1
    mov si, ax

    mov al, bh
    mov dl, TWICE_VGA_WIDTH
    mul dl
    mov bx, ax

    mov gs:[bx + si], cx

    ret

; Generate random position and return it in bx
random_position:
    mov bx, [rng_state]
    mov ax, bx
    shl ax, 7
    xor bx, ax
    mov cx, bx
    shr cx, 9
    xor bx, cx
    mov ax, bx
    shl ax, 8
    xor bx, ax
    mov [rng_state], bx
; Wrap position stored in bx
wrap_position:
    mov al, bh             ; Perform wrap on row value stored in bh
    mov cl, VGA_HEIGHT
    add al, cl             ; Add height since row can be -1 or 0xff
    xor ah, ah
    div cl
    mov bh, ah             ; Store wrapped row in bh

    mov al, bl             ; Perform wrap on col value stored in bl
    mov cl, VGA_WIDTH
    add al, cl             ; Add width since col can be -1 or 0xff
    xor ah, ah
    div cl
    mov bl, ah             ; Store wrapped col into bl
    ret

    ; Pad sector and add magic number
    times 510 - ($ - $$) db 0
    db 0x55, 0xaa
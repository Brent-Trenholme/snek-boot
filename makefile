boot.bin: boot.asm
	nasm -f bin boot.asm -o boot.bin

virt: boot.bin
	qemu-system-x86_64 boot.bin

.PHONY: virt